from langchain.prompts import (
    ChatPromptTemplate,
    MessagesPlaceholder,
    SystemMessagePromptTemplate,
    HumanMessagePromptTemplate,
)

from langchain.memory import ConversationBufferMemory
from langchain.chains import LLMChain

from langchain.llms import LlamaCpp

from langchain_experimental.sql import SQLDatabaseChain
from langchain.utilities import SQLDatabase
from art import *
import os

os.system('clear')
print("===================================================================[CASE STUDY: SQLDATABASECHAIN]==")
tprint("GenAI Chatbot", font="slant")
print("===================================================================================================")

llm = LlamaCpp(
    model_path="./models/llama-2-7b-chat.Q8_0.gguf",
    temperature=0.0,
    max_tokens=1024,
    n_ctx=5000,
    top_p=1,
    verbose=False, # Verbose is required to pass to the callback manager
)

# Prompt
prompt = ChatPromptTemplate(
    messages=[
        SystemMessagePromptTemplate.from_template("""
            MITRE ATLAS™ (Adversarial Threat Landscape for Artificial-Intelligence Systems) is a globally accessible, 
            living knowledge base of adversary tactics and techniques based on real-world attack observations and realistic 
            demonstrations from AI red teams and security groups. There are a growing number of vulnerabilities in AI-enabled 
            systems, as the incorporation of AI increases the attack surface of existing systems beyond those of traditional 
            cyber-attacks. We developed ATLAS to raise awareness of these unique and evolving vulnerabilities, as the global 
            community starts to incorporate AI into more systems. ATLAS is modeled after the MITRE ATT&CK® framework and its 
            tactics, techniques, and procedures (TTPs) are complementary to those in ATT&CK.
        """
        ),
        # The `variable_name` here is what must align with memory
        MessagesPlaceholder(variable_name="chat_history"),
        HumanMessagePromptTemplate.from_template("{question}"),
    ]
)

# Notice that we `return_messages=True` to fit into the MessagesPlaceholder
# Notice that `"chat_history"` aligns with the MessagesPlaceholder name
memory = ConversationBufferMemory(memory_key="chat_history", return_messages=True)

conversation = LLMChain(llm=llm, prompt=prompt, verbose=False, memory=memory)

db = SQLDatabase.from_uri("sqlite:///content/atlas.db")
db_chain = SQLDatabaseChain.from_llm(llm, db, verbose=True)

# Chatbot
human_input = ""

while "quit" not in human_input:
    human_input = input('\nYou > ')
    # Choose the appropriate source based on user input
    if "atlas" in human_input.lower():
        memory.chat_memory.add_user_message(human_input)
        ai_output = db_chain.run(human_input)
        memory.chat_memory.add_ai_message(ai_output)        
    else:
        conversation({"question": human_input})
    
    last_response = len(memory.chat_memory.messages) - 1
    print(memory.chat_memory.messages[last_response].content)
    